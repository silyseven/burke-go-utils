package logger

/*
Packet Usage:
Coding : UTF-8
Author : BurkeChing
Phone  : 17602109606
Mail   : silyseven@163.com
*/

import (
	"fmt"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func (l *Loger) InitZapLogger() *zap.SugaredLogger {
	//配置Zap的编码配置
	encoderConfig := zapcore.EncoderConfig{
		TimeKey:        "time",
		LevelKey:       "level",
		NameKey:        "logger",
		CallerKey:      "caller",
		MessageKey:     "msg",
		StacktraceKey:  "stacktracre",
		LineEnding:     zapcore.DefaultLineEnding,
		EncodeLevel:    zapcore.LowercaseLevelEncoder,
		EncodeTime:     zapcore.ISO8601TimeEncoder,
		EncodeDuration: zapcore.SecondsDurationEncoder,
		EncodeCaller:   zapcore.FullCallerEncoder,
	}

	//配置zap的日志配置
	zapconfig := zap.Config{
		Level:             zap.NewAtomicLevelAt(zap.DebugLevel),
		Development:       true,
		Encoding:          "console",
		EncoderConfig:     encoderConfig,
		InitialFields:     map[string]interface{}{"服务名称": l.ServiceName},
		OutputPaths:       []string{l.MessageLog},
		ErrorOutputPaths:  []string{l.ErrorLog},
		DisableCaller:     true,
		DisableStacktrace: true,
	}
	logger, err := zapconfig.Build()
	if err != nil {
		panic(fmt.Sprintf("初始化ZapLogger失败，请查看错误！err: %s", err))
	}
	fmt.Printf("加载ZapLogger日志模块成功！\n")
	return logger.Sugar()

}
