package configParse

import (
	"fmt"
	"github.com/spf13/viper"
)

/*
Packet Usage: 解析配置文件
Coding : UTF-8
Author : BurkeChing
Phone  : 17602109606
Mail  : silyseven@163.com
*/

//YamlReverse 进行yaml文件的解析。
func YamlReverse(configPath string, configStruct interface{}) error {
	fmt.Printf("开始解析配置文件，配置路径为:%s\n", configPath)
	//	调用viper进行解析
	c := viper.New()
	c.SetConfigFile(configPath)
	//	开始读取配置文件
	if err := c.ReadInConfig(); err != nil {
		fmt.Printf("配置文件读取失败，请检查配置文件错误！ error: %s\n", err)
		return err
	}

	//	将配置文件解析到传入的结构体
	if err := c.Unmarshal(configStruct); err != nil {
		fmt.Printf("解析配置文件到结构体错误！error:%s\n", err)
		return err
	}
	//	当处理都正常的时候，返回空。
	return nil
}
