package influx1_x

import (
	"fmt"
	"gitee.com/silyseven/burke-go-utils/logger"
	influx "github.com/influxdata/influxdb1-client/v2"
)

/*
Packet Usage:
Coding : UTF-8
Author : BurkeChing
Phone  : 17602109606
Mail   : silyseven@163.com
*/

func NewInfulx1(ServerIP, ServerPort, Username, Password string) *Influx1 {
	inflx := Influx1{}
	if len(ServerPort)&len(ServerIP)&len(Username)&len(Password) != 0 {
		inflx.ServerPort = ServerPort
		inflx.ServerIP = ServerIP
		inflx.Username = Username
		inflx.Password = Password
		inflx.IsLog = false //默认关闭Log功能
	} else {
		fmt.Println("输入的数据存在空值，请确认数据的值！")
		return nil
	}
	return &inflx
}

func (i *Influx1) SetMeasurement(measurements string) {
	i.Measurement = measurements
}

func (i *Influx1) SetDatabase(database string) {
	i.Database = database
}

func (i *Influx1) SetLog(isLog bool) {
	i.IsLog = isLog
}

func (i *Influx1) AddLogger(loger *logger.Loger) {
	i.Logger = loger
}

func (i *Influx1) Connect() error {
	//构造数据库连接
	client, err := influx.NewHTTPClient(influx.HTTPConfig{
		Addr:     fmt.Sprintf("http://%s:%s", i.ServerIP, i.ServerPort),
		Username: i.Username,
		Password: i.Password,
	})
	if err != nil {
		if i.IsLog == true {
			i.Logger.Loggger.Errorf("Connect to InfluxDB Server Error!  Error: %s\n", err)
		}
		return err
	}
	//连接正常，将Client存放到结构体
	i.Client = client
	return nil
}

func (i *Influx1) Disconnect() error {
	err := i.Client.Close()
	if err != nil {
		if i.IsLog == true {
			i.Logger.Loggger.Errorf("Disconnect Influxdb from Server Occuerd Error !  Error: %s\n", err)
		}
		return err
	}
	return nil
}
