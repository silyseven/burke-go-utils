package influx1_x

import (
	influx "github.com/influxdata/influxdb1-client/v2"
	"time"
)

/*
Packet Usage: 进行数据写入操作
Coding : UTF-8
Author : BurkeChing
Phone  : 17602109606
Mail  : silyseven@163.com
*/

func (i *Influx1) MakeBatchPoints(tags map[string]string, points map[string]interface{}) error {
	newBatchPoints, err := influx.NewBatchPoints(influx.BatchPointsConfig{
		Database:  i.Database,
		Precision: "s",
	})
	if err != nil {
		if i.IsLog == true {
			i.Logger.Loggger.Errorf("Create Influx BatchPoints Error! Error:%s\n", err)
		}
		return err
	}
	//	构造写入的数据点信息
	newpoint, err := influx.NewPoint(i.Measurement, tags, points, time.Time{})
	if err != nil {
		if i.IsLog == true {
			i.Logger.Loggger.Errorf("Make New Points Error! Error:%s\n", err)
		}
		return err
	}
	newBatchPoints.AddPoint(newpoint)
	i.BatchPoints = newBatchPoints
	return nil
}

func (i *Influx1) WriteBatchPoints() error {
	err := i.Client.Write(i.BatchPoints)
	if err != nil {
		if i.IsLog == true {
			i.Logger.Loggger.Errorf("Write BatchPoints to InfluxDB Error! Error:%s\n", err)
		}
		return err
	}
	//	如果没有错误的情况下
	if i.IsLog == true {
		i.Logger.Loggger.Infof("Write batchPoints to InfluxDB Success! Points is : %#v\n", i.BatchPoints)
	}
	return nil
}
