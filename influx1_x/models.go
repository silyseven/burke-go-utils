package influx1_x

import (
	"gitee.com/silyseven/burke-go-utils/logger"
	influx "github.com/influxdata/influxdb1-client/v2"
)

/*
Packet Usage: influxdb 1.x版本
Coding : UTF-8
Author : BurkeChing
Phone  : 17602109606
Mail   : silyseven@163.com
*/

type Influx1 struct {
	Username    string
	Password    string
	ServerIP    string
	ServerPort  string
	Database    string
	Measurement string
	Client      influx.Client
	Logger      *logger.Loger
	IsLog       bool
	BatchPoints influx.BatchPoints
}
