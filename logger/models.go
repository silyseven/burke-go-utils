package logger

import "go.uber.org/zap"

/*
Packet Usage: 相关loggere的参数
Coding : UTF-8
Author : BurkeChing
Phone  : 17602109606
Mail   : silyseven@163.com
*/

type Loger struct {
	ServiceName string
	MessageLog  string
	ErrorLog    string
	Loggger     *zap.SugaredLogger
}
