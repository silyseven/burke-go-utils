package logger

/*
Packet Usage:
Coding : UTF-8
Author : BurkeChing
Phone  : 17602109606
Mail   : silyseven@163.com
*/

//NewLogger 生成新的Log结构体
func NewLogger(ServiceName, MessageLog, ErrorLog string) *Loger {
	loger := Loger{
		ServiceName: ServiceName,
		MessageLog:  MessageLog,
		ErrorLog:    ErrorLog,
	}
	//初始化logg到Loger
	loger.Loggger = loger.InitZapLogger()
	return &loger

}
